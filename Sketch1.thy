theory Sketch1 imports Main "Lib.Lib" begin




(* basic types *)
type_synonym physAddr  = nat
type_synonym cacheAddr = nat
type_synonym colour    = nat
type_synonym domain    = nat







locale cache_config = 
  fixes physToCacheAddr :: "physAddr \<Rightarrow> cacheAddr"




locale coloured_cache_config = cache_config +
  fixes cacheToColour   :: "cacheAddr \<Rightarrow> colour"
  and   physToDomain    :: "physAddr \<Rightarrow> domain"
  assumes domainColoured : "cacheToColour(physToCacheAddr(pa1)) = cacheToColour(physToCacheAddr(pa2))
                          \<Longrightarrow> physToDomain(pa1) = physToDomain(pa2)"






(* helpers for binary stuff *)

definition getLowerBits :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
"getLowerBits num bits = num mod (2^bits)"

definition shiftDown :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
"shiftDown num bits = num div (2^bits)"

definition getMiddleBits :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat" where
"getMiddleBits num start bits = getLowerBits (shiftDown num start) bits"

value "getLowerBits  11 2"   (* 3 *)
value "shiftDown     11 2"   (* 2 *)
value "getMiddleBits 11 1 3" (* 5 *)





(* modeling binary cache stuff *)
locale binary_model = 
  fixes addressSize :: nat
  fixes cacheSize   :: nat
  fixes pageSize    :: nat
  fixes cacheAssoc  :: nat
  assumes sizesOK : "(pageSize < cacheSize) \<and> (cacheAssoc < (cacheSize - pageSize))"
begin
  (* constants depending on parameters *)
  definition cacheSets :: nat where "cacheSets = cacheSize - pageSize - cacheAssoc"

  (* functions depending on parameters *)
  definition getPageOffset  :: "physAddr \<Rightarrow> nat" where "getPageOffset n = getLowerBits n pageSize"
  definition getCacheIndex  :: "physAddr \<Rightarrow> nat" where "getCacheIndex n = getMiddleBits n pageSize cacheSets"
  definition getPhysicalTag :: "physAddr \<Rightarrow> nat" where "getPhysicalTag n = getMiddleBits n (pageSize+cacheSets) (addressSize-(pageSize+cacheSets))"

  interpretation binary_cache_model : cache_config getCacheIndex done
end



locale coloured_binary_model = binary_model +
  fixes colours   :: nat
  fixes getColour :: "physAddr \<Rightarrow> nat"
  assumes getColourOK : "getColour pa < colours"
  assumes colourDependsOnlyOnPhysicalTag : "\<exists> (getColourFromPhysicalTag :: physAddr \<Rightarrow> nat). getColour pa = getColourFromPhysicalTag (getPhysicalTag pa)"






sublocale coloured_binary_model \<subseteq> coloured_cache_config
  apply (unfold_locales)
  


(* interpretation coloured_binary_cache_config : coloured_cache_config  *)
















end
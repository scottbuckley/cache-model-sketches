theory Sketch3 imports Main begin

(* addresses and data *)
type_synonym address = nat
type_synonym data    = nat

(* memory is a simple function from addresses to data *)
type_synonym memory = "address \<Rightarrow> data"

(* hit or miss state *)
datatype cres = CHit data | CMiss data

(* instructions currently only read or do nothing *)
datatype instr = IRead address | IPass

(* arbitrary representation of time *)
type_synonym time = nat


(* instruction semantics with assumed cache_read behaviour *)
locale memory =
  fixes cache_read :: "memory \<Rightarrow> 'cache \<Rightarrow> address \<Rightarrow> cres \<times> 'cache"
begin
  fun exec1 :: "instr \<Rightarrow> memory \<Rightarrow> 'cache \<Rightarrow> (memory \<times> 'cache \<times> data \<times> time)" where
  "exec1 IPass m c = (m, c, 0, 0)" |
  "exec1 (IRead a) m c = (case (cache_read m c a) of
    (CHit  d, c') \<Rightarrow> (m, c', d, 0) |
    (CMiss d, c') \<Rightarrow> (m, c', d, 1)
  )"

  fun exec :: "instr list \<Rightarrow> memory \<Rightarrow> 'cache \<Rightarrow> (memory \<times> 'cache \<times> data \<times> time)" where
  "exec []    m c = (m, c, 0, 0)" |
  "exec [i]   m c = exec1 i m c" |
  "exec (i#r) m c = (case (exec1 i m c) of 
    (m', c', d', t') \<Rightarrow> (case (exec r m' c') of
      (m'', c'', d'', t'') \<Rightarrow> (m'', c'', d'', t'+t'')
    )
  )"
end


(* ASSOCIATIVE CACHE *)
(* the index of a cacheset *)
type_synonym cacheset_ind   = nat

(* a cacheset is a map from address to data *)
type_synonym cacheset = "address \<rightharpoonup> data"

(* a cache gives a cacheset for any cacheset index *)
type_synonym cache = "cacheset_ind \<Rightarrow> cacheset"

(*  *)

locale memory_cachesets_setassoc =
  (* associativity *)
  fixes z :: nat
  
  (* getting cacheset index from address *)
  fixes get_cacheset_ind :: "address \<Rightarrow> cacheset_ind"
  
  (* reading a cacheset *)
  fixes cacheset_read :: "memory \<Rightarrow> cacheset \<Rightarrow> address \<Rightarrow> cres \<times> cacheset"

  (* property that cache_read works within associativity bounds *)
  assumes cacheset_read_assoc : "card (dom (snd (cacheset_read m c a))) \<le> z"
begin

  (* cache_read implemented via cacheset_read and get_cacheset_ind *)
  fun cache_read :: "memory \<Rightarrow> cache \<Rightarrow> address \<Rightarrow> cres \<times> cache" where
  "cache_read m c a = (
    let ind = (get_cacheset_ind a) in (
    let cs = (c ind) in (
    let (res, cs') = (cacheset_read m cs a) in (
    (res, c(ind := cs'))
  ))))"

  
  (* cache_read preserves associativity *)
  lemma cache_read_assoc_aux : "(snd (cache_read m c a)) = (c((get_cacheset_ind a) := (snd (cacheset_read m (c (get_cacheset_ind a)) a))))"
  by (metis (no_types, lifting) cache_read.elims prod.exhaust prod.sel(2) prod.simps(2))

  theorem cache_read_assoc : "(\<forall> k. (card (dom (c k))) \<le> z) \<Longrightarrow> card (dom ((snd (cache_read m c a)) i)) \<le> z"
  using cacheset_read_assoc cache_read_assoc_aux by auto  
  

  (* this refines 'memory' *)
  sublocale memory cache_read done
end






end
theory Sketch2 imports Main "Lib.Lib" "HOL-Word.Word" begin


(*
  GENERAL GOAL
  
  abstract abstract:
  - domains in disjoint cachesets? will there be shared colours?
  - colours in disjoint cachesets?

  abstract model:
  - mappings from addresses to domains and colours (this is from a table, so always a variable?)
  - mappings from addresses to cachesets
  - proof that colours are in disjoint cachesets?

  less abstract mode:
  - concrete mappings from addresses to cachesets etc
  - any proof here? just an interpretation?

  

  - mappings from addresses to cache params - cache sets, cache index, etc
  - 


  my question:
  - the mapping between addresses and colours is provided by the kernel
  - what property of this mapping do we assume? what do we prove?
  - 

  - am i modeling the existing structure of the cache system?
  - am i modeling the high-level logic of cache colouring in general?
  - am i modeling a new structure for a cache system that is coloured?
  - 

  IF (concrete cache model has this property) THEN (no cache sets will be shared between domains) ?

  but the property is very simple. it's just about how addresses are allocated.





  you could add some primitives for : reading memory or writing to memory or something
  

  

  GERNOT AND COQ REVIEW
  check that the Coq claims are substantiated.
  Gernot may have questions about what they mean when they say something - hopefully you can have
  answers for these questions.
  maybe ask Gernot to share his review with you - he may or may not be happy to do that


*)






(* basic types *)
type_synonym cacheSet = int
type_synonym colour   = nat
type_synonym domain   = nat



(* we will parameterise everything by 'a, the address type *)
locale cache_config = 
  fixes physToCacheAddr :: "'a \<Rightarrow> cacheSet"

locale coloured_cache_config = cache_config +
  fixes cacheToColour   :: "cacheSet \<Rightarrow> colour"
  fixes physToDomain    :: "'a \<Rightarrow> domain"
  assumes domainColoured : "cacheToColour(physToCacheAddr(pa1)) = cacheToColour(physToCacheAddr(pa2))
                          \<Longrightarrow> physToDomain(pa1) = physToDomain(pa2)"







(* modeling binary cache stuff *)
locale word32_model = 
  fixes cacheSize   :: nat
  fixes pageSize    :: nat
  fixes cacheAssoc  :: nat
  assumes sizesOK : "(pageSize < cacheSize) \<and> (cacheAssoc < (cacheSize - pageSize))"
begin
  (* constants depending on parameters *)
  definition cacheSets :: nat where "cacheSets = cacheSize - pageSize - cacheAssoc"

  (* helpers *)
  definition getLowerBits32 :: "32 word \<Rightarrow> nat \<Rightarrow> int" where
  "getLowerBits32 addr bits = bintrunc bits (uint addr)"

  definition shiftDown32 :: "32 word \<Rightarrow> nat \<Rightarrow> int" where
  "shiftDown32 addr bits = uint (addr >> bits)"

  definition getMiddleBits32 :: "32 word \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> int" where
  "getMiddleBits32 addr start bits = getLowerBits32 (addr >> start) bits"

  (* functions depending on parameters *)
  definition getPageOffset  :: "32 word \<Rightarrow> int" where "getPageOffset n = getLowerBits32 n pageSize"
  definition getCacheIndex  :: "32 word Untitled-1\<Rightarrow> cacheSet" where "getCacheIndex n = getMiddleBits32 n pageSize cacheSets"
  definition getPhysicalTag :: "32 word \<Rightarrow> int" where "getPhysicalTag n = getMiddleBits32 n (pageSize+cacheSets) (32-(pageSize+cacheSets))"

  (* interpretation binary_cache_model : cache_config getCacheIndex done *)
  sublocale cache_config getCacheIndex done
end





locale coloured_word32_model = word32_model +
  fixes colours      :: nat
  fixes physToDomain :: "32 word  \<Rightarrow> domain"
  fixes cacheIndexToColour :: "cacheSet \<Rightarrow> colour"
  assumes coloursOK : "cacheIndexToColour ci < colours"
  assumes dcol : "(cacheIndexToColour (getCacheIndex pa1) = cacheIndexToColour (getCacheIndex pa2)) \<Longrightarrow> (physToDomain pa1 = physToDomain pa2)"
begin

sublocale coloured_cache_config getCacheIndex cacheIndexToColour physToDomain
  apply unfold_locales
  apply (rule dcol)
  apply clarsimp
done

end
  


































end